<?php
/**
 * Datacompressor\Exception\DataCompressorException
 */

namespace Datacompressor\Exception;

use Exception;

/**
 * Exception lors de l'utilisation de DataCompressor
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DataCompressorException extends Exception
{
}
