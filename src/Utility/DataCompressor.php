<?php
/**
 * Datacompressor\Utility\DataCompressor
 */

namespace Datacompressor\Utility;

use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Traversable;
use ZipArchive;
use Datacompressor\Exception\DataCompressorException;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use FilesystemIterator;

/**
 * Utilitaire de manipulation des archives (zip, rar, tar, gz)
 *
 * Utilise la même structure d'objet que \ZipArchive (en moins fourni)
 * @see http://php.net/manual/fr/class.ziparchive.php
 *
 * @category    Utility
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DataCompressor
{
    /**
     * @var array [UTF-8, ASCII, ISO-8859-1, ...]
     */
    const FILENAME_ENCODAGES = [
        'UTF-8', 'ASCII',
        'ISO-8859-1', 'ISO-8859-2', 'ISO-8859-3', 'ISO-8859-4', 'ISO-8859-5',
        'ISO-8859-6', 'ISO-8859-7', 'ISO-8859-8', 'ISO-8859-9', 'ISO-8859-10',
        'ISO-8859-13', 'ISO-8859-14', 'ISO-8859-15', 'ISO-8859-16',
        'Windows-1251', 'Windows-1252', 'Windows-1254',
    ];
    const DS = DIRECTORY_SEPARATOR;

    /**
     * @var string Encodage par défaut
     */
    public static $encodeFilename = 'UTF-8';

    /**
     * @var string namespace pour Filesystem
     */
    private static $transaction = 'no-transaction';

    /**
     * @var bool utilise ou pas \Libriciel\Filesystem\Utility\Filesystem
     */
    private static $useFilesystemClass;

    /**
     * @var bool utilise systématiquement la décompression en ligne de commande ?
     */
    public static bool $forceCommandLine = false;

    /**
     * Défini l'espace de nom à utiliser pour les transactions Filesystem
     *
     * @param string $namespace
     */
    public static function useTransaction(string $namespace = 'default')
    {
        static::$transaction = $namespace;
    }

    /**
     * Getter/Setter du useFilesystemClass
     * @param bool|null $use
     * @return bool
     */
    public static function useFilesystemClass(bool $use = null): bool
    {
        if ($use === false) {
            static::$useFilesystemClass = false;
        } elseif ($use === true || !isset(static::$useFilesystemClass)) {
            static::$useFilesystemClass = class_exists('Libriciel\Filesystem\Utility\Filesystem');
        }

        return static::$useFilesystemClass;
    }

    /**
     * Fonction générique de compression (compresse en zip)
     *
     * @param string|array|Traversable $files       A filename, an array of files, or a \Traversable instance
     * @param string                   $archiveName
     * @param string|null              $baseDir     ex: /tmp/dir1/file1 et /tmp/dir1/file2 avec
     *                                              /tmp en $baseDir donnera un zip avec
     *                                              dir1/file1 et dir1/file2. Par défaut,
     *                                              essayera de deviner à l'aide de $files
     * @param bool                     $overwrite
     * @return bool
     * @throws DataCompressorException
     */
    public static function compress($files, string $archiveName, string $baseDir = null, bool $overwrite = true): bool
    {
        self::checkTargetArchiveName($archiveName, $overwrite);
        if (is_string($files) && is_dir($files) && $baseDir === null) {
            $baseDir = $files;
        }
        $fileList = self::filesToArray($files);

        if (!$baseDir) {
            $baseDir = !static::useFilesystemClass()
                ? self::DS
                : Filesystem::baseDir($fileList);
        }
        // uniformise les baseDir finissent tous par /
        /** @noinspection PhpStrictComparisonWithOperandsOfDifferentTypesInspection faux positif */
        if (!substr($baseDir, - strlen(self::DS)) !== self::DS) {
            $baseDir .= self::DS;
        }

        return preg_match('/\.(tar\.gz|tgz)$/', $archiveName)
            ? self::tarGzFiles($files, $archiveName, $baseDir)
            : self::zipFiles($fileList, $archiveName, $baseDir);
    }

    /**
     * Compression en zip
     *
     * @param string|array|Traversable $files       A filename, an array of files, or a \Traversable instance
     * @param string                   $archiveName
     * @param string                   $baseDir
     * @return bool
     * @throws DataCompressorException
     */
    private static function zipFiles(array $files, string $archiveName, string $baseDir): bool
    {
        $zip = new ZipArchive;
        self::zipError($zip->open($archiveName, ZipArchive::CREATE | ZipArchive::OVERWRITE));

        foreach ($files as $file) {
            $addedFile = false;
            if (is_dir($file)) {
                continue;
            } elseif (is_readable($file)) {
                $addedFile = static::addFormatedFilename($zip, $file, $baseDir);
            } elseif (file_exists($file)) {
                trigger_error("File is not readable : ".$file);
            } elseif (!file_exists($file)) {
                trigger_error("File not found : ".$file);
            }
            if (!$addedFile) {
                throw new DataCompressorException("Erreur lors de l'ajout d'un fichier au zip");
            }
        }
        if (!$zip->close()) {
            throw new DataCompressorException("Erreur lors de la fermeture du fichier zip");
        }
        return true;
    }

    /**
     * Compression en GunZip (.tar.gz)
     * @param string|array|Traversable $files
     * @param string                   $archiveName
     * @param string                   $baseDir
     * @return bool
     * @throws DataCompressorException
     */
    private static function tarGzFiles($files, string $archiveName, string $baseDir): bool
    {
        $filesArr = [];
        $len = strlen($baseDir);
        if (!is_string($files)) {
            foreach ($files as $file) {
                if (!is_file($file)) {
                    throw new DataCompressorException('file not found ('.$file.')');
                } elseif (substr($file, 0, $len) !== $baseDir) {
                    throw new DataCompressorException('$baseDir is not in the $filename');
                }
                $relativeFilename = substr($file, $len);
                $filesArr[] = self::escapeshellarg($relativeFilename);
            }
            $fileArg = implode(' ', $filesArr);
        } else {
            // uniformise les dossiers finissent par /
            /** @noinspection PhpStrictComparisonWithOperandsOfDifferentTypesInspection faux positif */
            if (is_dir($files) && !substr($files, - strlen(self::DS)) !== self::DS) {
                $files .= self::DS;
            }
            if (substr($files, 0, $len) !== $baseDir) {
                throw new DataCompressorException('$baseDir is not in the $filename');
            }
            $relativeFilename = substr($files, $len);
            if ($relativeFilename) {
                $fileArg = self::escapeshellarg($relativeFilename);
            } else {
                $fileArg = '*';
                $hiddenFiles = array_filter(
                    is_dir($files) ? scandir($files) : [],
                    fn($v) => $v !== '.' && $v !== '..' && $v[0] === '.'
                );
                if ($hiddenFiles) {
                    $fileArg .= ' .[!.]*';
                }
            }
        }
        exec(
            sprintf(
                'cd %s && tar -chz %s -f %s 2>&1',
                self::escapeshellarg($baseDir),
                $fileArg,
                $archiveName
            ),
            $output,
            $code
        );
        if ($code !== 0) {
            trigger_error(implode("\n", $output));
        }
        return $code === 0;
    }

    /**
     * Décompresse une archive
     *
     * @param string    $path
     * @param string    $destination
     * @param null|bool $forceCommandLine
     * @return bool|array false si échec | array de nom de fichiers
     * @throws DataCompressorException
     */
    public static function uncompress(string $path, string $destination, bool $forceCommandLine = null)
    {
        $type = static::detectType($path);
        if (!$type) {
            throw new DataCompressorException("Le type de fichier n'est pas supporté");
        }
        $existing = self::checkDestination($destination);
        $uncompressDestination = !empty($existing)
            ? sys_get_temp_dir() . self::DS . uniqid('uncompress-destination-')
            : $destination;
        if (!is_dir($uncompressDestination)) {
            mkdir($uncompressDestination);
        }
        switch ($type) {
            case 'zip':
                $success = $forceCommandLine ?? static::$forceCommandLine
                    ? static::uncompressZipCmd($path, $uncompressDestination)
                    : static::uncompressZip($path, $uncompressDestination);
                break;
            case 'gzip':
                $success = static::uncompressGzip($path, $uncompressDestination);
                break;
            default:
                throw new DataCompressorException("Aucune méthode pour décompresser le type : ".$type);
        }
        if ($success) {
            if ($uncompressDestination !== $destination) {
                return static::safeMoveto($uncompressDestination, $destination);
            } else {
                return iterator_to_array(static::fileIterator($destination));
            }
        }
        return false;
    }

    /**
     * Déplace les fichiers en utilisant Filesystem si Disponible (pour les transactions)
     *
     * @param string $origin Dossier d'origine
     * @param string $target Dossier final
     * @return array
     * @throws Exception
     */
    private static function safeMoveto(string $origin, string $target): array
    {
        $targetDir = trim($target, self::DS);
        $files = iterator_to_array(
            static::fileIterator($origin)
        );
        $length = strlen(rtrim($origin, self::DS)) +1;
        $newFiles = [];

        if (static::useFilesystemClass()) {
            Filesystem::setNamespace(static::$transaction);
            foreach ($files as $file) {
                $newDir = self::DS . $targetDir . self::DS . substr($file, $length);
                $newFiles[] = $newDir;
                Filesystem::mkdir(dirname($newDir));
                Filesystem::rename($file, $newDir, true);
            }
            Filesystem::remove($origin);
        } else {
            foreach ($files as $file) {
                $newDir = self::DS . $targetDir . self::DS . substr($file, $length);
                $newFiles[] = $newDir;
                if (file_exists($newDir)) {
                    unlink($newDir);
                }
                static::mkdir(dirname($newDir));
                rename($file, $newDir);
            }
            rmdir($origin);
        }
        return $newFiles;
    }

    /**
     * Créer les dossiers de façon récursive
     *
     * @param string $dir
     */
    private static function mkdir(string $dir)
    {
        $path = '';
        foreach (explode(self::DS, trim($dir, self::DS)) as $dirName) {
            $path .= self::DS . $dirName;
            if (!is_dir($path)) {
                mkdir($path);
            }
        }
    }

    /**
     * Donne la liste complète des fichiers d'un répertoire
     *
     * @param string $dirname
     * @return RecursiveIteratorIterator
     */
    private static function fileIterator(string $dirname): RecursiveIteratorIterator
    {
        $flags = FilesystemIterator::CURRENT_AS_PATHNAME
            | FilesystemIterator::SKIP_DOTS;
        return new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dirname, $flags)
        );
    }

    /**
     * Converti et normalise les noms de fichier en UTF-8
     * et supprime le dossier __MACOSX
     * @param string $path
     * @return bool
     * @throws DataCompressorException
     */
    public static function sanitizeZip(string $path): bool
    {
        $zip = new ZipArchive;
        if (!$zip->open($path)) {
            throw new DataCompressorException("Impossible d'ouvrir le fichier zip");
        }
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $indexName = $zip->getNameIndex($i);
            $filename = static::formatFilename($indexName, 'UTF-8');
            if ($filename !== $indexName) {
                $zip->renameIndex($i, $filename);
            }
        }
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $indexName = $zip->getNameIndex($i);
            if (!$indexName) {
                continue;
            }
            if (strpos($indexName, '__MACOSX') !== false) {
                $zip->deleteIndex($i);
                $i--; // NOSONAR cas particulier
            }
        }
        if (!$zip->close()) {
            throw new DataCompressorException("Erreur lors de la fermeture du fichier zip");
        }
        return true;
    }

    /**
     * Décompresse une archive zip
     *
     * @param string $path
     * @param string $destination
     * @return bool success
     * @throws DataCompressorException
     */
    private static function uncompressZip(string $path, string $destination): bool
    {
        $zip = new ZipArchive;
        if (!$zip->open($path)) {
            throw new DataCompressorException("Impossible d'ouvrir le fichier zip");
        }
        for($i = 0; $i < $zip->numFiles; $i++){
            $stat = $zip->statIndex($i);
            $filename = $stat['name'];
            if ($stat['comp_method'] === ZipArchive::CM_DEFLATE64){
                $data = @gzinflate($zip->getFromIndex($i, 0, ZipArchive::FL_COMPRESSED));
                if ($data === false) {
                    return self::uncompressZipCmd($path, $destination);
                }
                file_put_contents("$destination/$filename", $data);
            } elseif (!self::safeExtractTo($zip, $destination, $filename)) {
                throw new DataCompressorException("Impossible d'extraire le fichier zip");
            }
        }
        if (!$zip->close()) {
            throw new DataCompressorException("Erreur lors de la fermeture du fichier zip");
        }
        return true;
    }

    /**
     * Un fulldisk peut donner un true sur extractTo() avec une partie des
     * fichiers manquants. Cette fonction permet d'envoyer une exception dans ce
     * cas.
     * @param ZipArchive  $zip
     * @param string      $destination
     * @param string|null $filename
     * @return bool
     * @throws DataCompressorException
     */
    private static function safeExtractTo(ZipArchive $zip, string $destination, string $filename = null): bool
    {
        set_error_handler(
            function ($errno, $errstr) {
                if ($errno === E_WARNING || $errno === E_NOTICE) {
                    throw new DataCompressorException($errstr);
                }
                return false;
            }
        );
        try {
            $success = $zip->extractTo($destination, $filename);
        } finally {
            restore_error_handler();
        }
        return $success;
    }

    /**
     * Décompresse une archive zip à partir d'une ligne de commande
     *
     * @param string $path
     * @param string $destination
     * @return bool success
     */
    private static function uncompressZipCmd(string $path, string $destination): bool
    {
        exec(
            sprintf(
                'unzip %s -d %s',
                self::escapeshellarg($path),
                self::escapeshellarg($destination)
            ),
            $output,
            $code
        );
        return $code === 0;
    }

    /**
     * Décompresse une archive tar.gz
     *
     * NOTE : Peut altérer son contenu (supprime __MAXOSX et renomme en UTF-8)
     * La sauvegarde du fichier serait possible mais couteuse en ressources
     *
     * @param string $path
     * @param string $destination
     * @return bool success
     */
    private static function uncompressGzip(string $path, string $destination): bool
    {
        exec(
            sprintf(
                'tar xfa %s --directory %s',
                self::escapeshellarg($path),
                self::escapeshellarg($destination)
            ),
            $output,
            $code
        );
        return $code === 0;
    }

    /**
     * Ajoute au zip le fichier/dossier $filename formaté selon
     * l'encodage choisi et $baseDir retiré
     *
     * @param ZipArchive $zip
     * @param string     $filename
     * @param string     $baseDir
     * @return bool
     */
    public static function addFormatedFilename(ZipArchive $zip, string $filename, string $baseDir = '/'): bool
    {
        $normalized = static::formatFilename($filename);
        $inzipName = trim(substr($normalized, strlen($baseDir)), self::DS);

        if (is_dir($filename)) {
            return $zip->addEmptyDir($inzipName);
        }
        return $zip->addFile($filename, $inzipName);
    }

    /**
     * Renvoi un nom de fichier encodé en UTF-8 et normalisé
     *
     * L'encodage UTF-8 : Peut être modifié dans la configuration sous la
     * clef DataCompressor.encodeFilename
     *
     * @param string      $filename
     * @param string|null $format
     * @return string
     */
    public static function formatFilename(string $filename, string $format = null): string
    {
        $useFormat = $format ?: self::$encodeFilename;
        /** @noinspection PhpFullyQualifiedNameUsageInspection */
        if (class_exists('\Cake\Core\Configure')
            && $conf = \Cake\Core\Configure::read('DataCompressor.encodeFilename')
        ) {
            $useFormat = $conf;
        }
        $filenameFormat = mb_detect_encoding($filename, static::FILENAME_ENCODAGES);

        $formated = $useFormat && $useFormat !== $filenameFormat
            ? iconv($filenameFormat, $useFormat.'//IGNORE', $filename)
            : $filename;
        $normalized = normalizer_is_normalized($formated)
            ? $formated
            : normalizer_normalize($formated);
        $normalized = preg_replace('#\.+/#', '/', $normalized);
        return str_replace("\n", '', $normalized);
    }

    /**
     * Permet de détecter le type d'archive
     *
     * @param string $filename
     * @return boolean|string false|zip|gzip
     * @phpcs:disable Generic.Metrics.CyclomaticComplexity
     */
    public static function detectType(string $filename)
    {
        if (is_file($filename)) {
            switch (mime_content_type($filename)) {
                case 'application/x-zip':
                case 'application/x-zip-compressed':
                case 'application/zip':
                case 'application/zip-compressed':
                case 'multipart/x-zip':
                    $ext = 'zip';
                    break;
                case 'application/x-rar':
                case 'application/x-rar-compressed':
                    $ext = 'rar';
                    break;
                case 'application/gzip':
                case 'application/tar+gzip':
                case 'application/x-gzip':
                case 'application/x-tgz':
                case 'application/x-gtar':
                case 'application/tar':
                case 'application/x-tar':
                case 'multipart/x-tar':
                    $ext = 'gzip';
                    break;
                default:
                    $ext = false;
            }
        } else {
            switch ($ext = pathinfo($filename, PATHINFO_EXTENSION)) {
                case 'rar':
                case 'zip':
                    break;
                case 'tar':
                case 'tgz':
                case 'gz':
                    $ext = 'gzip';
                    break;
                default:
                    return false;
            }
        }

        return $ext;
    }

    /**
     * Donne le nombre de fichiers dans un dossier compréssé
     * @param string $path
     * @return int
     * @throws DataCompressorException
     */
    public static function fileCount(string $path): int
    {
        return self::detectType($path) === 'zip' ? self::zipFileCount($path) : 0;
    }

    /**
     * Donne le nombre de fichiers dans un zip
     * @param string $path
     * @return int
     * @throws DataCompressorException
     */
    private static function zipFileCount(string $path)
    {
        $zip = new ZipArchive;
        if (!$zip->open($path)) {
            throw new DataCompressorException("Impossible d'ouvrir le fichier zip");
        }
        return $zip->count();
    }

    /**
     * Envoi une exception avec le message selon error
     * @param int|true $error
     * @return bool
     * @throws DataCompressorException
     */
    public static function zipError($error)
    {
        /** @link ZipArchive::open() Returns <b>TRUE</b> on success or the error code. */
        if ($error === true) {
            $error = ZipArchive::ER_OK;
        }
        $map = [
            ZipArchive::ER_MULTIDISK => "Multi-disk zip archives not supported.",
            ZipArchive::ER_RENAME => "Renaming temporary file failed.",
            ZipArchive::ER_CLOSE => "Closing zip archive failed",
            ZipArchive::ER_SEEK => "Seek error",
            ZipArchive::ER_READ => "Read error",
            ZipArchive::ER_WRITE => "Write error",
            ZipArchive::ER_CRC => "CRC error",
            ZipArchive::ER_ZIPCLOSED => "Containing zip archive was closed",
            ZipArchive::ER_NOENT => "No such file.",
            ZipArchive::ER_EXISTS => "File already exists.",
            ZipArchive::ER_OPEN => "Can't open file",
            ZipArchive::ER_TMPOPEN => "Failure to create temporary file.",
            ZipArchive::ER_ZLIB => "Zlib error",
            ZipArchive::ER_MEMORY => "Memory allocation failure",
            ZipArchive::ER_CHANGED => "Entry has been changed",
            ZipArchive::ER_COMPNOTSUPP => "Compression method not supported.",
            ZipArchive::ER_EOF => "Premature EOF",
            ZipArchive::ER_INVAL => "Invalid argument",
            ZipArchive::ER_NOZIP => "Not a zip archive",
            ZipArchive::ER_INTERNAL => "Internal error",
            ZipArchive::ER_INCONS => "Zip archive inconsistent",
            ZipArchive::ER_REMOVE => "Can't remove file",
            ZipArchive::ER_DELETED => "Entry has been deleted",
            ZipArchive::ER_ENCRNOTSUPP => "Encryption method not support",
            ZipArchive::ER_RDONLY => "Read-only archive",
            ZipArchive::ER_NOPASSWD => "No password provided",
            ZipArchive::ER_WRONGPASSWD => "Wrong password provided",
            ZipArchive::ER_OPNOTSUPP => "Operation not supported",
            ZipArchive::ER_INUSE => "Resource still in use",
            ZipArchive::ER_TELL => "Tell error",
            ZipArchive::ER_COMPRESSED_DATA => "Compressed data invalid",
            defined(ZipArchive::class . 'ER_CANCELLED')
                ? ZipArchive::ER_CANCELLED
                : 32 => "Operation cancelled",
        ];
        if (isset($map[$error])) {
            throw new DataCompressorException($map[$error]);
        }
        return true;
    }

    /**
     * Vérifi le dossier de destination
     * @param string $destination
     * @return array|RecursiveIteratorIterator
     * @throws DataCompressorException
     */
    private static function checkDestination(string $destination)
    {
        if (!is_dir($destination)) {
            if (file_exists($destination)) {
                throw new DataCompressorException(
                    sprintf("Le dossier de destination porte le même nom qu'un fichier existant : %s", $destination)
                );
            }
            if (!static::useFilesystemClass()) {
                throw new DataCompressorException("Le dossier de destination d'existe pas");
            }
            $existing = [];
            Filesystem::setNamespace(static::$transaction);
            Filesystem::mkdir($destination);
        } else {
            $existing = static::fileIterator($destination);
        }
        if (!is_writable($destination)) {
            throw new DataCompressorException(
                sprintf("Le dossier de destination n'est pas inscriptible : %s", $destination)
            );
        }
        return $existing;
    }

    /**
     * Vérifi le dossier de $archiveName
     * @param string $archiveName
     * @param bool   $overwrite
     * @throws DataCompressorException
     */
    private static function checkTargetArchiveName(string $archiveName, bool $overwrite)
    {
        if (!is_dir(dirname($archiveName))) {
            if (!static::useFilesystemClass()) {
                throw new DataCompressorException("Le dossier de destination du zip n'existe pas");
            }
            Filesystem::setNamespace(static::$transaction);
            Filesystem::mkdir(dirname($archiveName));
        }
        if (file_exists($archiveName)) {
            if (!$overwrite) {
                throw new DataCompressorException("Un fichier du même nom existe déjà !");
            } elseif (static::useFilesystemClass()) {
                Filesystem::setNamespace(static::$transaction);
                Filesystem::revertFileIfRollback($archiveName);
            }
        } elseif (static::useFilesystemClass()) {
            Filesystem::setNamespace(static::$transaction);
            Filesystem::addToDeleteIfRollback($archiveName);
        }
    }

    /**
     * Donne un array de fichiers
     * @param string|array|Traversable $files
     * @return string[]
     * @throws DataCompressorException
     */
    private static function filesToArray($files): array
    {
        if (is_string($files)) {
            if (is_file($files)) {
                $files = [$files];
            } elseif (is_dir($files)) {
                $files = iterator_to_array(static::fileIterator($files));
            } else {
                $files = (array)$files;
            }
        } elseif ($files instanceof Traversable) {
            $files = iterator_to_array($files, false);
        }
        foreach ($files as $file) {
            if (!is_file($file)) {
                throw new DataCompressorException('file not found ('.$file.')');
            }
        }
        return $files;
    }

    /**
     * Permet d'échanper un paramètre sans retirer les accents
     * @param string $arg
     * @return string
     */
    public static function escapeshellarg(string $arg): string
    {
        setlocale(LC_CTYPE, 'en_US.UTF-8'); // pour gérer les accents dans la fonction escapeshellarg()
        $escaped = escapeshellarg($arg);
        setlocale(LC_ALL, null);
        return $escaped;
    }
}
