<?php

namespace Datacompressor\Test\TestCase\Utility;

use Datacompressor\Exception\DataCompressorException;
use Datacompressor\Utility\DataCompressor;
use Libriciel\Filesystem\Utility\Filesystem;
use PHPUnit\Framework\TestCase;
use ZipArchive;

class DataCompressorTest extends TestCase
{
    const TMP_FILES_DIR = 'testunit-datacompressor';
    const TMP_ARCHIVE_DIR = 'testunit-datacompressor-archives';

    public function setUp(): void
    {
        if (!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }
        if (!defined('TMP')) {
            define('TMP', sys_get_temp_dir() . DS);
        }
        if (class_exists('\Cake\Core\Configure')) {
            /** @noinspection PhpFullyQualifiedNameUsageInspection PhpUndefinedNamespaceInspection */
            \Cake\Core\Configure::write('DataCompressorUtility.encodeFilename', false);
        }
        DataCompressor::$encodeFilename = false;
        DataCompressor::$forceCommandLine = false;
        DataCompressor::useFilesystemClass(true);
    }

    public function tearDown(): void
    {
        exec('rm '.escapeshellarg(sys_get_temp_dir() . DS . self::TMP_FILES_DIR).' -R 2>&1');
        exec('rm '.escapeshellarg(sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR).' -R 2>&1');
    }

    public function testDetectType()
    {
        $this->assertEquals("zip", DataCompressor::detectType('test.zip'), "Detecte ZIP");
        $this->assertEquals(false, DataCompressor::detectType('test.php'), "Detecte PHP mais renvoi false");
        $this->assertEquals(false, DataCompressor::detectType('test'), "Ne detecte pas d'extension, renvoi false");
        $this->assertEquals("rar", DataCompressor::detectType('test.rar'), "Detecte RAR");

        $tmpDir = sys_get_temp_dir() . DS . self::TMP_FILES_DIR;
        if (!is_dir($tmpDir)) {
            mkdir($tmpDir);
        }

        foreach (['UTF-8//TRANSLIT', 'IBM850//TRANSLIT', 'ASCII//TRANSLIT'] as $enc) {
            $file = tempnam($tmpDir, preg_replace('/[\W]/', '_', $enc).'-é@ùµ$`à-');
            file_put_contents($file, "UTF8 é@ùµ$`à");
            $this->assertFalse(DataCompressor::detectType($file), "Le fichier existe mais n'est pas une archive");

            $zip = new \ZipArchive;
            $zip->open($file.'.zip', \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
            $zip->addFile($file, iconv('UTF-8', $enc.'//IGNORE', basename($file)));
            $zip->close();
            $this->assertEquals('zip', DataCompressor::detectType($file.'.zip'), "Le fichier existe");
        }
    }

    public function testZip()
    {
        $files = $this->generateFileTree();
        $zipName = sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR . DS . 'test-zip.zip';
        DataCompressor::compress($files, $zipName);
        $this->assertTrue(is_file($zipName), "Vérification de l'existance du zip");
        $this->assertNotEmpty(filesize($zipName), "Vérification de la taille du zip");

        DataCompressor::uncompress($zipName, sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR);
        $filename = sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR . DS . basename($files[0]);
        $this->assertTrue(is_file($filename), "Vérification de l'existance du fichier dézippé");
        $this->assertEquals(file_get_contents($filename), file_get_contents($files[0]), "Le contenu du fichier ne doit pas changer");

        exec('rm '.escapeshellarg(sys_get_temp_dir() . DS . self::TMP_FILES_DIR).' -R 2>&1');
        exec('rm '.escapeshellarg(sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR).' -R 2>&1');

        // Essai avec noms à problème et directory plutot que liste de fichiers
        $files = $this->generateProblematicFileTree();
        DataCompressor::compress(sys_get_temp_dir() . DS . self::TMP_FILES_DIR . DS . 'problematic', $zipName);
        $this->assertTrue(is_file($zipName), "Vérification de l'existance du zip 2");
        $this->assertNotEmpty(filesize($zipName), "Vérification de la taille du zip 2");

        // On change l'encodage des fichiers du zip pour le test suivant
        $zip = new \ZipArchive;
        $zip->open($zipName);
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $indexName = $zip->getNameIndex($i);
            $reformated = iconv('UTF-8', 'IBM850', $indexName);
            $zip->renameIndex($i, $reformated);
        }
        $zip->close();

        DataCompressor::sanitizeZip($zipName);
        DataCompressor::uncompress($zipName, sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR);
        $filename = sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR . DS . basename($files[0]);
        $this->assertTrue(is_file($filename), "Vérification de l'existance du fichier dézippé 2");
        $this->assertEquals(file_get_contents($filename), file_get_contents($files[0]), "Le contenu du fichier ne doit pas changer 2");
        $this->assertEquals('UTF-8', mb_detect_encoding($filename, DataCompressor::FILENAME_ENCODAGES), "L'encodage du nom de fichier doit être UTF-8");
    }

    public function testRollback()
    {
        $files = $this->generateFileTree();
        $zipName = sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR . DS . 'test-zip.zip';

        FileSystem::begin();
        DataCompressor::useTransaction();
        DataCompressor::compress($files, $zipName);

        $this->assertTrue(is_file($zipName), "Le zip existe avant rollback");

        FileSystem::rollback();

        $this->assertFalse(is_file($zipName), "Le zip n'existe plus après rollback");
    }

    public function testGzip()
    {
        $files = $this->generateFileTree();
        $tmpDir = sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR;
        $tarName = $tmpDir . DS . 'test-tar-gz.tar';
        $gzName = $tmpDir . DS . 'test-tar-gz.tar.gz';
        $tgzName = $tmpDir . DS . 'test-tar-gz.tgz';
        if (!is_dir($tmpDir)) {
            mkdir($tmpDir);
        }

        $gzip = new \PharData($tarName);
        $length = strlen(sys_get_temp_dir() . DS . self::TMP_FILES_DIR);
        foreach ($files as $file) {
            $normalized = DataCompressor::formatFilename($file);
            $inzipName = trim(substr($normalized, $length), DS);
            $gzip->addFile($file, $inzipName);
        }
        $gzip->compress(\Phar::GZ);
        Filesystem::copy($gzName, $tgzName);

        $this->assertFileExists($tarName, "Vérification de l'existance du tar");
        $this->assertFileExists($gzName, "Vérification de l'existance du tar.gz");
        $this->assertFileExists($tgzName, "Vérification de l'existance du tgz");

        $destTar = $tmpDir . DS . 'tar';
        $destGz = $tmpDir . DS . 'gz';
        $destTgz = $tmpDir . DS . 'tgz';

        // Debut du vrai test
        $filesTar = DataCompressor::uncompress($tarName, $destTar);
        $filesGz = DataCompressor::uncompress($gzName, $destGz);
        $filesTgz = DataCompressor::uncompress($tgzName, $destTgz);

        $this->assertNotEmpty($filesTar, 'Fichiers Tar extrait');
        $this->assertNotEmpty($filesGz, 'Fichiers TarGz extrait');
        $this->assertNotEmpty($filesTgz, 'Fichiers TGz extrait');
        $this->assertFileExists($destTar, "Vérification de l'existance du tar n°2");

        foreach (array_merge($filesTar, $filesGz, $filesTgz) as $file) {
            $this->assertTrue(is_file($file) && filesize($file) > 0, "Vérifi l'existance de ".$file);
            $inArray = false;
            foreach ($files as $filename) {
                if (basename($filename) === basename($file)) {
                    $inArray = true;
                    $this->assertEquals(file_get_contents($filename), file_get_contents($file), "Le contenu n'a pas changé");
                    break;
                }
            }
            $this->assertTrue($inArray, "Le fichier a bien été trouvé : ".$file . ' | ' . var_export($files, true));
        }
    }

    protected function generateFileTree()
    {
        $tmpDir = sys_get_temp_dir() . DS . self::TMP_FILES_DIR;

        $dirs = [
            $tmpDir,
            $tmpDir . DS . 'dir1',
            $tmpDir . DS . 'dir2',
            $tmpDir . DS . 'dir3',
            $tmpDir . DS . 'dir3' . DS . 'dir4',
        ];
        foreach ($dirs as $dir) {
            if (!is_dir($dir)) {
                mkdir($dir);
            }
        }

        $files = [];
        $files[] = tempnam($tmpDir, 'file01-');
        $files[] = tempnam($tmpDir, 'file02-');
        $files[] = tempnam($tmpDir . DS . 'dir1', 'file03-');
        $files[] = tempnam($tmpDir . DS . 'dir1', 'file04-');
        $files[] = tempnam($tmpDir . DS . 'dir2', 'file05-');
        $files[] = tempnam($tmpDir . DS . 'dir3' . DS . 'dir4', 'file06-');

        foreach ($files as $file) {
            file_put_contents($file, uniqid());
        }

        return $files;
    }

    protected function generateProblematicFileTree()
    {
        $tmpDir = sys_get_temp_dir() . DS . self::TMP_FILES_DIR . DS . 'problematic';
        if (!is_dir(sys_get_temp_dir() . DS . self::TMP_FILES_DIR)) {
            mkdir(sys_get_temp_dir() . DS . self::TMP_FILES_DIR);
        }

        $dirs = [
            $tmpDir,
            $tmpDir . DS . 'diràé1',
            $tmpDir . DS . '__MAXOSX',
        ];
        $files = [];
        foreach ($dirs as $dir) {
            if (!is_dir($dir)) {
                mkdir($dir);
            }
            FileSystem::createDummyFile($dir . DS . 'filàé', 200);
            $files[] = $dir . DS . 'filàé';
        }

        foreach ($files as $file) {
            $this->assertTrue(is_file($file), $file);
        }

        return $files;
    }

    public function testCompress()
    {
        $dirZip = sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR;
        $dirFiles = sys_get_temp_dir() . DS . self::TMP_FILES_DIR;
        $files = new \ArrayObject([$dirFiles.DS.'foo.bar']);
        $zipname = $dirZip.DS.'test.zip';

        // Test conflit overwrite
        Filesystem::dumpFile($zipname, 'foo');
        try {
            @DataCompressor::compress($files, $zipname);
            $this->fail();
        } catch (DataCompressorException $e) {
            $this->assertTrue(true);
        }

        // test zip folder
        Filesystem::dumpfile($dirFiles.DS.'foo.bar', 'foo');
        symlink($dirFiles.DS.'foo.bar', $dirFiles.DS.'foo.biz');
        DataCompressor::compress($dirFiles, $zipname = $dirZip.DS.'test'.DS.'test.zip');
        $this->assertFileExists($zipname);
        $md5 = md5_file($zipname);

        // test single file
        Filesystem::dumpfile($dirFiles.DS.'foo.bar', 'bar');
        DataCompressor::compress($dirFiles.DS.'foo.bar', $zipname = $dirZip.DS.'test'.DS.'test.zip');
        $this->assertFileExists($zipname);
        $this->assertNotEquals($md5, md5_file($zipname));

        // test sans Filesystem
        DataCompressor::useFilesystemClass(false);
        Filesystem::remove(dirname($zipname));
        try {
            DataCompressor::compress($files, $zipname);
            $this->fail();
        } catch (DataCompressorException $e) {
            $this->assertTrue(true);
        }
        Filesystem::dumpFile($zipname, 'foo');
        try {
            DataCompressor::compress($files, $zipname, null, false);
            $this->fail();
        } catch (DataCompressorException $e) {
            $this->assertTrue(true);
        }
        Filesystem::remove($zipname);
        DataCompressor::compress($files, $zipname, null, false);
        $this->assertFileExists($zipname);

        chmod($dirFiles.DS.'foo.bar', 0111);
        try {
            @DataCompressor::compress($files, $zipname);
            $this->fail();
        } catch (\Exception $e) {
            $this->assertTrue(true);
        } finally {
            chmod($dirFiles.DS.'foo.bar', 0777);
        }
    }

    public function testCompressTarGz()
    {
        $dirZip = sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR;
        $dirFiles = sys_get_temp_dir() . DS . self::TMP_FILES_DIR;

        // test tar.gz folder
        Filesystem::dumpfile($dirFiles.DS.'foo.bar', 'foo');
        symlink($dirFiles.DS.'foo.bar', $dirFiles.DS.'foo.biz');
        DataCompressor::compress($dirFiles, $tarname = $dirZip.DS.'test'.DS.'test.tar.gz');
        $this->assertFileExists($tarname);
        $md5 = md5_file($tarname);

        // test symlink
        DataCompressor::uncompress($tarname, $dirZip . DS . 'test2');
        $this->assertFalse(is_link($dirZip . DS . 'test2' . DS . 'foo.biz'));

        // test single file
        Filesystem::dumpfile($dirFiles.DS.'foo.bar', 'bar');
        DataCompressor::compress($dirFiles.DS.'foo.bar', $tarname2 = $dirZip.DS.'test'.DS.'test2.tar.gz');
        $this->assertFileExists($tarname2);
        $this->assertNotEquals($md5, md5_file($tarname2));

        // test array files
        DataCompressor::compress([$tarname, $tarname2], $tarname3 = $dirZip.DS.'test'.DS.'test3.tar.gz');
        $this->assertFileExists($tarname3);
    }

    public function testCountFiles()
    {
        $dirZip = sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR;
        $dirFiles = sys_get_temp_dir() . DS . self::TMP_FILES_DIR;
        Filesystem::dumpfile($dirFiles.DS.'foo.bar', 'foo');
        symlink($dirFiles.DS.'foo.bar', $dirFiles.DS.'foo.biz');
        DataCompressor::compress($dirFiles, $dirZip.DS.'test'.DS.'test.zip');

        $this->assertEquals(2, DataCompressor::fileCount($dirZip.DS.'test'.DS.'test.zip'));
    }

    public function testZipError()
    {
        DataCompressor::zipError(ZipArchive::ER_OK); // pas d'exception

        $errors = [];
        $lastError = 32; // ZipArchive::ER_CANCELLED
        for ($i = 1; $i <= $lastError; $i++) {
            $error = null;
            try {
                DataCompressor::zipError($i);
                $this->fail("Error $i not thrown");
            } catch (DataCompressorException $e) {
                $error = $e->getMessage();
            }
            $this->assertNotEmpty($error);
            $this->assertFalse(in_array($error, $errors));
            $errors[] = $error;
        }
    }

    public function testUnzipDeflate64()
    {
        $zipName = dirname(__DIR__, 2) . '/Data/deflate64.zip';
        $files = DataCompressor::uncompress($zipName, sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR);
        foreach ($files as $file) {
            $this->assertTrue(is_file($file), "Vérification de l'existance du fichier dézippé");
        }
    }

    public function testUncompressZipCmd()
    {
        $zipName = dirname(__DIR__, 2) . '/Data/deflate64.zip';
        $files = DataCompressor::uncompress($zipName, sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR, true);
        foreach ($files as $file) {
            $this->assertTrue(is_file($file), "Vérification de l'existance du fichier dézippé");
        }
    }

    public function testUncompressZipCmdDefault()
    {
        DataCompressor::$forceCommandLine = true;
        $zipName = dirname(__DIR__, 2) . '/Data/deflate64.zip';
        $files = DataCompressor::uncompress($zipName, sys_get_temp_dir() . DS . self::TMP_ARCHIVE_DIR);
        foreach ($files as $file) {
            $this->assertTrue(is_file($file), "Vérification de l'existance du fichier dézippé");
        }
    }
}
