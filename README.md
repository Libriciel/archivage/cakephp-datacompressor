# Data Compressor
[![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor/pipelines/latest)
[![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-datacompressor/badges/master/coverage.svg)](https://asalae2.dev.libriciel.fr/coverage/cakephp-datacompressor/index.html)

## Description

Permet de compresser/décompresser des fichiers de differents type (zip, rar, gz, ...), avec prise en charge des transactions (plugin Filesystem).

## Installation

```bash
composer config repositories.libriciel/cakephp-datacompressor git https://gitlab.libriciel.fr/lganee/cakephp-datacompressor.git
composer require libriciel/cakephp-datacompressor ~1.0
```

## Utilisation

En PSR-4, ajoutez un `use Datacompressor\Utility\DataCompressor;`

### Compression

```php
DataCompressor::compress($dir, $zipName);
```

### Décompression

```php
DataCompressor::uncompress($zipName, $dir);
```

### Transactions

```php
Filesystem::begin();
DataCompressor::useTransaction();
DataCompressor::uncompress($zipName, $dir);
Filesystem::commit();
```

## Tests

```bash
vendor/bin/phpunit
```